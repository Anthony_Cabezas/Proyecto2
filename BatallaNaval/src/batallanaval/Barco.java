/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

/**
 *
 * @author Fabricio
 */
public class Barco {

    private int tipo;
    private String horientacion;
    private int impacto;

    public Barco() {
        this.impacto = 0;
    }

    public Barco(int tipo, String horientacion) {
        this.tipo = tipo;
        this.horientacion = horientacion;    
    }

    public boolean recibirImpacto() {
        impacto++;
        return this.tipo == impacto;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getHorientacion() {
        return horientacion;
    }

    public void setHorientacion(String horientacion) {
        this.horientacion = horientacion;
    }

    @Override
    public String toString() {
        return "Barco " + "tipo=" + tipo + ", horientacion=" + horientacion + ", impacto=" + impacto + "\n";
    }

}
