/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Fabricio
 */
public class MiRender extends DefaultTableCellRenderer {

    public Component getTableCellRendererComponent(JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {
         JLabel cell = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if (value instanceof Integer) {
            int temp = (Integer) value;

            if (temp != -1) {
                cell.setOpaque(true);
                cell.setBackground(Color.RED);
                cell.setForeground(Color.WHITE);

            } else {
                Color c = new Color(0,255,204);
                cell.setBackground(c);
                cell.setForeground(c);
            }
        }
        return this;
    }
}
