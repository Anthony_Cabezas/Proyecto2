/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.util.Arrays;
import javax.swing.JTable;

/**
 *
 * @author Fabricio
 */
public class Tablero {

    private int[][] jugador;
    private int[][] pc;
    private Barco[] barco;
    private Barco[] barcoPC;
    private boolean turno;//true : Jugador, false : pc

    public Tablero() {
        this.jugador = new int[10][10];
        this.pc = new int[10][10];
        this.barco = new Barco[5];
        this.barcoPC = new Barco[5];
        this.turno = true;
        this.inicializar();
        this.cargarBarco();
        this.cargarBarcoPC();
        this.colocarBarcosPc();
    }

    /**
     * Inicializa los datos en las matrices.
     */
    private void inicializar() {
        for (int i = 0; i < jugador.length; i++) {
            for (int j = 0; j < jugador[i].length; j++) {
                this.jugador[i][j] = -1;
                this.pc[i][j] = -1;
            }
        }
    }

    /**
     * Agrega barcos al tablero del jugador.
     *
     * @param horientacion La horientacion del barco.
     * @param tipo El tipo del barco.
     * @param fila La fila donde se va a agregar.
     * @param columna La columna donde se va a agregar.
     * @return
     */
    public String agregarBarco(String horientacion, int tipo, int fila, int columna) {
        String msj = "";
        barco[tipo - 1].setHorientacion(horientacion);
        if (horientacion(tipo).equalsIgnoreCase("horizontal")) {
            if ((horizontalCabe1(jugador, tipo, fila, columna)) || (horizontalCabe2(jugador, tipo, fila, columna))) {
                msj = "Barco Agreado";
            } else {
                msj = "NO cabe";
                barco[tipo - 1].setHorientacion(null);
            }
        } else if (horientacion(tipo).equalsIgnoreCase("vertical")) {
            if ((verticalCabe1(jugador, tipo, fila, columna)) || (verticalCabe2(jugador, tipo, fila, columna))) {
                msj = "Barco Agreado";
            } else {
                msj = "NO cabe";
                barco[tipo - 1].setHorientacion(null);
            }
        }

        return msj;
    }

    /**
     * Ataca por turnos.
     *
     * @param fila La fila que va a atacar.
     * @param columna La columna que va a atacar.
     * @return String Mensaje de donde ataco.
     */
    public String atacar(int fila, int columna) {
        String msj1 = "";
        int contaJugador = 0;
        int contaPC = 0;
        if (turno) {
            this.turno = false;
            if (pc[fila][columna] != -1) {
                msj1 += "Usuario Ataco a (" + (fila + 1) + "," + (columna + 1) + "): Fuego\n";
                for (int i = 0; i < barcoPC.length; i++) {
                    if (barcoPC[i].getTipo() == pc[fila][columna]) {
                        if (barcoPC[i].recibirImpacto()) {
                            contaJugador++;
                        }
                    }
                }
            } else {
                msj1 += "Usuario Ataco a (" + (fila + 1) + "," + (columna + 1) + "): Agua\n";
            }
        }
        if (!turno) {
            this.turno = true;
            int fil = (int) (Math.random() * 10);
            int col = (int) (Math.random() * 10);
            if (jugador[fil][col] != -1) {
                msj1 += "PC Atacó a (" + (fil + 1) + "," + (col + 1) + "): Fuego\n";
                for (int i = 0; i < barco.length; i++) {
                    if (barco[i].getTipo() == jugador[fil][col]) {
                        if (barco[i].recibirImpacto()) {
                            contaPC++;
                        }
                    }
                }
            } else {
                msj1 += "PC Atacó a (" + (fil + 1) + "," + (col + 1) + "): Agua\n";
            }
        }
        if (contaJugador == 5) {
            msj1 = "Usuario Gano";
        } else if (contaPC == 5) {
            msj1 = "PC Gano";
        }

        return msj1;
    }

    /**
     * Carga los datos de la matriz jugador en la JTable.
     *
     * @param tabla
     * @return
     */
    public JTable mostrarTablero(JTable tabla) {
        tabla.setDefaultRenderer(Object.class, new MiRender());
        for (int i = 0; i < jugador.length; i++) {
            for (int j = 0; j < jugador[i].length; j++) {
                tabla.setValueAt(jugador[i][j], i, j);
            }
        }
        return tabla;
    }

    public JTable mostrarTableroPC(JTable tabla) {
        tabla.setDefaultRenderer(Object.class, new MiRender());
        for (int i = 0; i < pc.length; i++) {
            for (int j = 0; j < pc[i].length; j++) {
                tabla.setValueAt(pc[i][j], i, j);
            }
        }
        return tabla;
    }

    public int[][] getJugador() {
        return jugador;
    }

    public int[][] getPc() {
        return pc;
    }

    /**
     * Agrega datos por defecto al arreglo barco.
     */
    private void cargarBarco() {
        barco[0] = new Barco(1, null);
        barco[1] = new Barco(2, null);
        barco[2] = new Barco(3, null);
        barco[3] = new Barco(4, null);
        barco[4] = new Barco(5, null);
    }

    /**
     * Agrega datos por defecto al arreglo barcoPC.
     */
    private void cargarBarcoPC() {
        barcoPC[0] = new Barco(1, null);
        barcoPC[1] = new Barco(2, null);
        barcoPC[2] = new Barco(3, null);
        barcoPC[3] = new Barco(4, null);
        barcoPC[4] = new Barco(5, null);
    }

    /**
     * Verifica si hay campo en la matriz hacia la derecha.
     *
     * @param matriz La matriz que deseamos verificar.
     * @param tipo Tipo del barco.
     * @param fila La fila de la matriz.
     * @param columna La columna de la matriz
     * @return Boolean True si cabe, False sino.
     */
    private boolean horizontalCabe1(int matriz[][], int tipo, int fila, int columna) {
        if ((tipo + columna) <= 10) {
            if (camposVacios(matriz, tipo, fila, columna, 1)) {
                for (int i = columna; i < tipo + columna; i++) {
                    matriz[fila][i] = tipo;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica si hay campo en la matriz hacia izquierda.
     *
     * @param matriz La matriz que deseamos verificar.
     * @param tipo Tipo del barco.
     * @param fila La fila de la matriz.
     * @param columna La columna de la matriz
     * @return Boolean True si cabe, False sino.
     */
    private boolean horizontalCabe2(int matriz[][], int tipo, int fila, int columna) {
        if (columna - tipo >= 0) {
            if (camposVacios(matriz, tipo, fila, columna, 2)) {
                for (int i = columna; i > (columna - tipo); i--) {
                    matriz[fila][i] = tipo;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica si hay campo en la matriz hacia abajo.
     *
     * @param matriz La matriz que deseamos verificar.
     * @param tipo Tipo del barco.
     * @param fila La fila de la matriz.
     * @param columna La columna de la matriz
     * @return
     */
    private boolean verticalCabe1(int matriz[][], int tipo, int fila, int columna) {
        if ((tipo + fila) <= 10) {
            if (camposVacios(matriz, tipo, fila, columna, 3)) {
                for (int i = fila; i < fila + tipo; i++) {
                    matriz[i][columna] = tipo;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica si hay campo en la matriz hacia arriba.
     *
     * @param matriz La matriz que deseamos verificar.
     * @param tipo Tipo del barco.
     * @param fila La fila de la matriz.
     * @param columna La columna de la matriz
     * @return Boolean True si cabe, False sino.
     */
    private boolean verticalCabe2(int matriz[][], int tipo, int fila, int columna) {
        if ((fila - tipo) >= 0) {
            if (camposVacios(matriz, tipo, fila, columna, 4)) {
                for (int i = fila; i > fila - tipo; i--) {
                    matriz[i][columna] = tipo;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Revisa si los a campos estan vacios.
     *
     * @param tipo El tipo de barco.
     * @param fila La fila donde se va a colocar.
     * @param columna La columna donde se a a colocar.
     * @param direccion Direccion del barco.
     * 1:Derecha,2:Izquierda,3:Abajo,4:Arriba
     *
     * @return true si se puede guardar, false sino.
     */
    private boolean camposVacios(int matriz[][], int tipo, int fila, int columna, int direccion) {
        int conta = 0;
        boolean cabe = false;
        switch (direccion) {
            case 1:
                for (int i = columna; i < columna + tipo; i++) {
                    if (matriz[fila][i] == -1) {
                        conta++;
                        if (conta == tipo) {
                            cabe = true;
                        }
                    }
                }
                break;
            case 2:
                for (int i = columna; i > columna - tipo; i--) {
                    if (matriz[fila][i] == -1) {
                        conta++;
                        if (conta == tipo) {
                            cabe = true;
                        }
                    }
                }
                break;
            case 3:
                for (int i = fila; i < fila + tipo; i++) {
                    if (matriz[i][columna] == -1) {
                        conta++;
                        if (conta == tipo) {
                            cabe = true;
                        }
                    }
                }
                break;
            case 4:
                for (int i = fila; i > fila - tipo; i--) {
                    if (matriz[i][columna] == -1) {
                        conta++;
                        if (conta == tipo) {
                            cabe = true;
                        }
                    }
                }
                break;
            default:
                break;
        }
        return cabe;
    }

//    private int tomarTipo(int tipo) {
//        int dato = -1;
//        for (int i = 0; i < barco.length; i++) {
//            if (barco[i].getTipo() == tipo) {
//                dato = barco[i].getTipo();
//            }
//        }
//        return dato;
//    }
    /**
     * Toma la horientacion del barco.
     *
     * @param tipo Tipo de barco.
     * @return String Horientacion del barco.
     */
    private String horientacion(int tipo) {
        String msj = "";
        for (int i = 0; i < barco.length; i++) {
            if (barco[i].getTipo() == tipo) {
                msj = barco[i].getHorientacion();
            }
        }
        return msj;
    }

    /**
     * Verfica si todas las horientaciones son diferentes a null para iniciar el
     * juego.
     *
     * @return Boolean.
     */
    public boolean nulos() {
        boolean nulo = false;
        int conta = 0;
        for (int i = 0; i < barco.length; i++) {
            if (barco[i].getHorientacion() != null) {
                conta++;
                if (conta == 5) {
                    nulo = true;
                }
            }
        }
        return nulo;
    }

    /**
     * Coloca los barcos de la pc en la matriz apenas inicia el juego.
     */
    private void colocarBarcosPc() {
        String[] horientacion = {"Horizontal", "Vertical"};
        for (int i = 0; i < barcoPC.length; i++) {
            int rand = (int) (Math.random() * 2);
            barcoPC[i].setHorientacion(horientacion[rand]);
            boolean agregado = true;
            while (agregado) {
                int fila = (int) (Math.random() * 10);
                int col = (int) (Math.random() * 10);
                if (barcoPC[i].getHorientacion().equalsIgnoreCase("horizontal")) {
                    if ((horizontalCabe1(pc, barcoPC[i].getTipo(), fila, col)) || (horizontalCabe2(pc, barcoPC[i].getTipo(), fila, col))) {
                        agregado = false;
                    }
                } else if (barcoPC[i].getHorientacion().equalsIgnoreCase("vertical")) {
                    if ((verticalCabe1(pc, barcoPC[i].getTipo(), fila, col)) || (verticalCabe2(pc, barcoPC[i].getTipo(), fila, col))) {
                        agregado = false;
                    }
                }
            }
        }

    }

    public Barco[] getBarco() {
        return barco;
    }

}
